$(function() {
    function VisualizerViewModel(parameters) {
        var self = this;
  
	self.settings = parameters[0];
	self.state = parameters[1];
  
	//self.color_input1 = ko.observable();
	//self.color_input2 = ko.observable();
	
	self.file = ko.observable(undefined);
	
	self.file = self.state.filename;
  
    self.progress = ko.observable(undefined);

    self.webcam_view = ko.observable(false); 	
    self.gcode_view = ko.observable(true);
	
	self.webcam_flipH = ko.computed(function() {
 		return parameters[0].webcam_flipH();
  	}); 
	self.webcam_flipV = ko.computed(function() {
 		return parameters[0].webcam_flipV();
  	}); 
	self.isFileLoaded = ko.computed(function(){
	  if (ko.toJS(self.file) === "undefined" || ko.toJS(self.file) === null )
	  	return false;
	  if(scene !== null){
		if (scene.children.slice(-1).pop().name !== ko.toJS(self.file)) 
			return true;
		else
			return false;
	  }
	  return true;
	});

	self.toggle_webcam_view = function() {
		self.webcam_view(true);
		self.gcode_view(false);
		}
	self.toggle_gcode_view = function() {
		self.webcam_view(false);
		self.gcode_view(true);
		}
  
    self.savedColor1 = ko.computed(function() {
        var color1 = self.settings.model_color1();
        return color1;
    });
    self.savedColor2 = ko.computed(function() {
        var color2 = self.settings.model_color2();
        return color2;
    });
  
    self.fromCurrentData = function(data) {
        self._fromData(data);
    };
  
    self.fromHistoryData = function(data) {
        self._fromData(data);
    };
  
    self._fromData = function(data) {
        self._processProgressData(data.progress);
    };

  
    self._processProgressData = function(data) {
        if (data.completion) {
            self.progress(data.completion);
        } else {
            self.progress(undefined);
        }
    };
  
    self.render_progress = function() {
        if(render_progresses === undefined || render_progresses === null)
            self.progress(undefined);
        else
            self.progress(render_progresses);
        if(self.progress() === undefined)
            return "Error";
        return self.progress();
    }

	
	// openGCodeFromPath(ko.toJS(self.state.filename));
   	}
    /*if(!scene)
        scene = createScene($('#renderArea'));*/

 

    OCTOPRINT_VIEWMODELS.push([
        VisualizerViewModel,
        ["settingsViewModel","printerStateViewModel"],
        "#visualizer_wrapper"
    ]);
});
