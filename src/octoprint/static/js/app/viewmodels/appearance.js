$(function() {
    function AppearanceViewModel(parameters) {
        var self = this;

        self.name = parameters[0].appearance_name;
        self.color = parameters[0].appearance_color;
        self.colorTransparent = parameters[0].appearance_colorTransparent;
        self.model_color1 = parameters[0].model_color1;
        self.model_color2 = parameters[0].model_color2;
	
	self.machine_name = ko.computed(function() {
	   if(self.name())
		return self.name();
	   else
		return gettext("Series 1");
	});
        self.brand = ko.computed(function() {
            if (self.name())
                return gettext("Series 1") + ": " + self.name();
            else
                return gettext("Series 1");
        });

        self.title = ko.computed(function() {
            if (self.name())
                return self.name() + " - " + gettext("Type A Machines"); 
            else
                return gettext("Series 1");
        });
    }

    OCTOPRINT_VIEWMODELS.push([
        AppearanceViewModel,
        ["settingsViewModel"],
        "head"
    ]);
});
