/**
 * @author Eberhard Graether / http://egraether.com/
 * @author Mark Lundin 	/ http://mark-lundin.com
 * @author Simone Manini / http://daron1337.github.io
 * @author Luca Antiga 	/ http://lantiga.github.io
 */

THREE.TrackballControls = function ( object, domElement ) {
    
    var _this = this;
    var STATE = { NONE: -1, ROTATE: 0, ZOOM: 1, PAN: 2, TOUCH_ROTATE: 3, TOUCH_ZOOM_PAN: 4 };
    
    this.object = object;
    this.domElement = ( domElement !== undefined ) ? domElement : document;
    
    // API
    
    this.enabled = true;
    
    this.screen = { left: 0, top: 0, width: 0, height: 0 };
    
    this.rotateSpeed = 1.0;
    this.zoomSpeed = 1.2;
    this.panSpeed = 0.3;
    
    this.noRotate = false;
    this.noZoom = false;
    this.noPan = false;
    
    this.staticMoving = false;
    this.dynamicDampingFactor = 0.2;
    
    this.minDistance = 0;
    this.maxDistance = Infinity;
    
    this.keys = [ 65 /*A*/, 83 /*S*/, 68 /*D*/ ];
    
    // internals
    
    this.target = new THREE.Vector3();
    
    var EPS = 0.000001;
    
    var lastPosition = new THREE.Vector3();
    
    var _state = STATE.NONE,
    _prevState = STATE.NONE,
    
    _eye = new THREE.Vector3(),
    
    _movePrev = new THREE.Vector2(),
    _moveCurr = new THREE.Vector2(),
    
    _lastAxis = new THREE.Vector3(),
    _lastAngle = 0,
    
    _zoomStart = new THREE.Vector2(),
    _zoomEnd = new THREE.Vector2(),
    
    _touchZoomDistanceStart = 0,
    _touchZoomDistanceEnd = 0,
    
    _panStart = new THREE.Vector2(),
    _panEnd = new THREE.Vector2();
    
    // for reset
    
    this.target0 = this.target.clone();
    this.position0 = this.object.position.clone();
    this.up0 = this.object.up.clone();
    
    // events
    
    var changeEvent = { type: 'change' };
    var startEvent = { type: 'start' };
    var endEvent = { type: 'end' };
    
    
    // methods
    
    this.handleResize = function () {
        
        if ( this.domElement === document ) {
            
            this.screen.left = 0;
            this.screen.top = 0;
            this.screen.width = window.innerWidth;
            this.screen.height = window.innerHeight;
            
        } else {
            
            var box = this.domElement.getBoundingClientRect();
            // adjustments come from similar code in the jquery offset() function
            var d = this.domElement.ownerDocument.documentElement;
            this.screen.left = box.left + window.pageXOffset - d.clientLeft;
            this.screen.top = box.top + window.pageYOffset - d.clientTop;
            this.screen.width = box.width;
            this.screen.height = box.height;
            
        }
        
    };
    
    this.handleEvent = function ( event ) {
        
        if ( typeof this[ event.type ] == 'function' ) {
            
            this[ event.type ]( event );
            
        }
        
    };
    
    var getMouseOnScreen = ( function () {
                            
                            var vector = new THREE.Vector2();
                            
                            return function ( pageX, pageY ) {
                            
                            vector.set(
                                       ( pageX - _this.screen.left ) / _this.screen.width,
                                       ( pageY - _this.screen.top ) / _this.screen.height
                                       );
                            
                            return vector;
                            
                            };
                            
                            }() );
    
    var getMouseOnCircle = ( function () {
                            
                            var vector = new THREE.Vector2();
                            
                            return function ( pageX, pageY ) {
                            
                            vector.set(
                                       ( ( pageX - _this.screen.width * 0.5 - _this.screen.left ) / ( _this.screen.width * 0.5 ) ),
                                       ( ( _this.screen.height + 2 * ( _this.screen.top - pageY ) ) / _this.screen.width ) // screen.width intentional
                                       );
                            
                            return vector;
                            };
                            
                            }() );
    
    this.rotateCamera = (function() {
                         
        var axis = new THREE.Vector3(),
            quaternion = new THREE.Quaternion(),
            eyeDirection = new THREE.Vector3(),
            objectUpDirection = new THREE.Vector3(),
            objectSidewaysDirection = new THREE.Vector3(),
            moveDirection = new THREE.Vector3(),
            angle;
                         
        return function () {
                         
            moveDirection.set( _moveCurr.x - _movePrev.x, _moveCurr.y - _movePrev.y, 0 );
            angle = moveDirection.length();
                         
            if ( angle ) {
                         
                _eye.copy( _this.object.position ).sub( _this.target );
                         
                eyeDirection.copy( _eye ).normalize();
                objectUpDirection.copy( _this.object.up ).normalize();
                objectSidewaysDirection.crossVectors( objectUpDirection, eyeDirection ).normalize();
                         
                objectUpDirection.setLength( _moveCurr.y - _movePrev.y );
                objectSidewaysDirection.setLength( _moveCurr.x - _movePrev.x );
                         
                moveDirection.copy( objectUpDirection.add( objectSidewaysDirection ) );
                         
                axis.crossVectors( moveDirection, _eye ).normalize();
                         
                angle *= _this.rotateSpeed;
                quaternion.setFromAxisAngle( axis, angle );
                         
                _eye.applyQuaternion( quaternion );
                _this.object.up.applyQuaternion( quaternion );
                         
                _lastAxis.copy( axis );
                _lastAngle = angle;
                         
            }
                         
            else if ( !_this.staticMoving && _lastAngle ) {
                         
                _lastAngle *= Math.sqrt( 1.0 - _this.dynamicDampingFactor );
                _eye.copy( _this.object.position ).sub( _this.target );
                quaternion.setFromAxisAngle( _lastAxis, _lastAngle );
                _eye.applyQuaternion( quaternion );
                _this.object.up.applyQuaternion( quaternion );
                         
            }
                         
            _movePrev.copy( _moveCurr );
            
            // Changing Rendering Order for Gcode Transparency Rendering
            if(gcode_object && view_mode_3d && (angle || (!_this.staticMoving && _lastAngle))) {
                var line_array = gcode_object.getDescendants();
                var mlength = -1;
                var depth_counter = 0;
                var zero_array = [];
                for(var cid in  line_array) {
                    var child = line_array[cid];
                    if(child.material.visible) {
                        var child_z = (child.geometry.vertices[0].z * curr_model.scale) + gcode_object.position.z;
                        var z_dif = _this.object.position.z - child_z;
                        if(z_dif < 0) {
                            child.renderDepth = (-z_dif);
                        } else if(z_dif > 0) {
                            child.renderDepth = z_dif;
                        } else {
                            child.renderDepth = 0;
                            //zero_array.push(child);
                        }
                    }
                }
                
                // Case: z_dif is zero, doesn't really work :( will work on it in the future
                /*if(zero_array.length > 0) {
                    eyeDirection.copy( _eye ).normalize();
                    objectUpDirection = new THREE.Vector3(0,0,1);
                    objectSidewaysDirection.crossVectors( objectUpDirection, eyeDirection ).normalize();
                    var side_dir = new THREE.Vector2(objectSidewaysDirection.x,objectSidewaysDirection.y);
                    var cam_pos = new THREE.Vector2(_this.object.position.x,_this.object.position.y);
                    var dist_a = new THREE.Vector2();
                    var dist_b = new THREE.Vector2();
                    var other_a = new THREE.Vector2();
                    var other_b = new THREE.Vector2();
                    var e2p_a0 = new THREE.Vector2();
                    var e2p_a1 = new THREE.Vector2();
                    var dist_a0 = new THREE.Vector2();
                    var dist_a1 = new THREE.Vector2();
                    var e2p_b0 = new THREE.Vector2();
                    var e2p_b1 = new THREE.Vector2();
                    var dist_b0 = new THREE.Vector2();
                    var dist_b1 = new THREE.Vector2();
                    zero_array.sort(function(a,b) {
                        var apos0 = a.geometry.vertices[0].clone();
                        var bpos0 = b.geometry.vertices[0].clone();
                        var apos1 = a.geometry.vertices[1].clone();
                        var bpos1 = b.geometry.vertices[1].clone();
                        apos0.multiplyScalar(3);
                        apos1.multiplyScalar(3);
                        apos0.add(gcode_object.position);
                        apos1.add(gcode_object.position);
                        bpos0.multiplyScalar(3);
                        bpos1.multiplyScalar(3);
                        bpos0.add(gcode_object.position);
                        bpos1.add(gcode_object.position);
                        var a0 = new THREE.Vector2(apos0.x,apos0.y);
                        var a1 = new THREE.Vector2(apos1.x,apos1.y);
                        var b0 = new THREE.Vector2(bpos0.x,bpos0.y);
                        var b1 = new THREE.Vector2(bpos1.x,bpos1.y);
                        e2p_a0.subVectors(a0,cam_pos);
                        e2p_a1.subVectors(a1,cam_pos);
                        e2p_b0.subVectors(b0,cam_pos);
                        e2p_b1.subVectors(b1,cam_pos);
                        var side_scale_a0 = e2p_a0.dot(side_dir);
                        var side_scale_a1 = e2p_a1.dot(side_dir);
                        var side_scale_b0 = e2p_b0.dot(side_dir);
                        var side_scale_b1 = e2p_b1.dot(side_dir);
                        var sidev_a0 = side_dir.clone();
                        var sidev_a1 = side_dir.clone();
                        var sidev_b0 = side_dir.clone();
                        var sidev_b1 = side_dir.clone();
                        sidev_a0.multiplyScalar(side_scale_a0);
                        sidev_a1.multiplyScalar(side_scale_a1);
                        sidev_b0.multiplyScalar(side_scale_b0);
                        sidev_b1.multiplyScalar(side_scale_b1);
                        dist_a0.subVectors(e2p_a0,sidev_a0);
                        dist_a1.subVectors(e2p_a1,sidev_a1);
                        if(dist_a0.length() > dist_a1.length()) {
                            dist_a = dist_a0;
                            other_a = dist_a1;
                        } else {
                            dist_a = dist_a1;
                            other_a = dist_a0;
                        }
                        //dist_a = (dist_a0.length() > dist_a1.length() ? dist_a0 : dist_a1);
                        dist_b0.subVectors(e2p_b0,sidev_b0);
                        dist_b1.subVectors(e2p_b1,sidev_b1);
                        if(dist_b0.length() > dist_b1.length()) {
                            dist_b = dist_b0;
                            other_b = dist_b1;
                        } else {
                            dist_b = dist_b1;
                            other_b = dist_b0;
                        }
                        //dist_b = (dist_b0.length() > dist_b1.length() ? dist_b0 : dist_b1);
                        //console.log("a value is: " + e2p_a0.length() + " side_scale_a0 is: " + side_scale_a0 + " sidev_a0 is: " + sidev_a0.length() + " side to a is: " + dist_a0.length());
                        if(dist_a.length() - dist_b.length() != 0) {
                            return (dist_a.length() - dist_b.length());
                        } else {
                            if(other_a.length() - other_b.length() != 0) {
                                return (other_a.length() - other_b.length());
                            } else {
                                
                            }
                        }
                    });
                    
                    for(i = 0; i < zero_array.length; i++) {
                        if(zero_array[i]) {
                            zero_array[i].renderDepth = depth_counter+= 0.01;
                        }
                        //console.log("zero_array value " + i + " is " + zero_array[i].geometry.vertices[0].x + " , " + zero_array[i].geometry.vertices[0].y + "   ;   " + zero_array[i].geometry.vertices[1].x + " , " + zero_array[i].geometry.vertices[1].y + "  ; with depth counter: " + zero_array[i].renderDepth);
                    }
                }*/
            }
        };
                         
    }());
    
    
    this.zoomCamera = function () {
        
        var factor;
        
        if ( _state === STATE.TOUCH_ZOOM_PAN ) {
            
            factor = _touchZoomDistanceStart / _touchZoomDistanceEnd;
            _touchZoomDistanceStart = _touchZoomDistanceEnd;
            _eye.multiplyScalar( factor );
            
        } else {
            
            factor = 1.0 + ( _zoomEnd.y - _zoomStart.y ) * _this.zoomSpeed;
            
            if ( factor !== 1.0 && factor > 0.0 ) {
                
                _eye.multiplyScalar( factor );
                
                if ( _this.staticMoving ) {
                    
                    _zoomStart.copy( _zoomEnd );
                    
                } else {
                    
                    _zoomStart.y += ( _zoomEnd.y - _zoomStart.y ) * this.dynamicDampingFactor;
                    
                }
                
            }
            
        }
        
        if(curr_model && curr_model.snap && view_mode_3d && gcode_object) {
            if(layer_counter3d < gcode_layers.length)
                snap_gap = _eye.length() - (gcode_layers[layer_counter3d].z * curr_model.scale + gcode_object.position.z + curr_model.world_topz / 2);
            else
                snap_gap = _eye.length() - (gcode_layers[layer_counter3d-1].z * curr_model.scale + gcode_object.position.z + curr_model.world_topz / 2);
        }
        
    };
    
    this.panCamera = (function() {
                      
                      var mouseChange = new THREE.Vector2(),
                      objectUp = new THREE.Vector3(),
                      pan = new THREE.Vector3();
                      
                      return function () {
                      
                      mouseChange.copy( _panEnd ).sub( _panStart );
                      
                      if ( mouseChange.lengthSq() ) {
                      
                      mouseChange.multiplyScalar( _eye.length() * _this.panSpeed );
                      
                      pan.copy( _eye ).cross( _this.object.up ).setLength( mouseChange.x );
                      pan.add( objectUp.copy( _this.object.up ).setLength( mouseChange.y ) );
                      
                      _this.object.position.add( pan );
                      _this.target.add( pan );
                      
                      if ( _this.staticMoving ) {
                      
                      _panStart.copy( _panEnd );
                      
                      } else {
                      
                      _panStart.add( mouseChange.subVectors( _panEnd, _panStart ).multiplyScalar( _this.dynamicDampingFactor ) );
                      
                      }
                      
                      }
                      };
                      
                      }());
    
    this.checkDistances = function () {
        
        if ( !_this.noZoom || !_this.noPan ) {
            
            if ( _eye.lengthSq() > _this.maxDistance * _this.maxDistance ) {
                
                _this.object.position.addVectors( _this.target, _eye.setLength( _this.maxDistance ) );
                
            }
            
            if ( _eye.lengthSq() < _this.minDistance * _this.minDistance ) {
                
                _this.object.position.addVectors( _this.target, _eye.setLength( _this.minDistance ) );
                
            }
            
        }
        
    };
    
    this.update = function () {
        
        _eye.subVectors( _this.object.position, _this.target );
        
        if ( !_this.noRotate ) {
            
            _this.rotateCamera();
            
        }
        
        if ( !_this.noZoom ) {
            
            _this.zoomCamera();
            
        }
        
        if ( !_this.noPan ) {
            
            _this.panCamera();
            
        }
        
        _this.object.position.addVectors( _this.target, _eye );
        
        _this.checkDistances();
        
        _this.object.lookAt( _this.target );
        
        if ( lastPosition.distanceToSquared( _this.object.position ) > EPS ) {
            
            _this.dispatchEvent( changeEvent );
            
            lastPosition.copy( _this.object.position );
            
        }
        
    };
    
    this.reset = function () {
        
        _state = STATE.NONE;
        _prevState = STATE.NONE;
        
        _this.target.copy( _this.target0 );
        _this.object.position.copy( _this.position0 );
        _this.object.up.copy( _this.up0 );
        
        _eye.subVectors( _this.object.position, _this.target );
        
        _this.object.lookAt( _this.target );
        
        _this.dispatchEvent( changeEvent );
        
        lastPosition.copy( _this.object.position );
        
    };
    
    // listeners
    
    function keydown( event ) {
        
        if ( _this.enabled === false ) return;
        
        window.removeEventListener( 'keydown', keydown );
        
        _prevState = _state;
        
        if ( _state !== STATE.NONE ) {
            
            return;
            
        } else if ( event.keyCode === _this.keys[ STATE.ROTATE ] && !_this.noRotate ) {
            
            _state = STATE.ROTATE;
            
        } else if ( event.keyCode === _this.keys[ STATE.ZOOM ] && !_this.noZoom ) {
            
            _state = STATE.ZOOM;
            
        } else if ( event.keyCode === _this.keys[ STATE.PAN ] && !_this.noPan ) {
            
            _state = STATE.PAN;
            
        }
        
    }
    
    function keyup( event ) {
        
        if ( _this.enabled === false ) return;
        
        _state = _prevState;
        
        window.addEventListener( 'keydown', keydown, false );
        
    }
    
    function mousedown( event ) {
        
        if ( _this.enabled === false ) return;
        
        event.preventDefault();
        event.stopPropagation();
        
        if ( _state === STATE.NONE ) {
            
            _state = event.button;
            
        }
        
        if ( _state === STATE.ROTATE && !_this.noRotate ) {
            
            _moveCurr.copy( getMouseOnCircle( event.pageX, event.pageY ) );
            _movePrev.copy(_moveCurr);
            
        } else if ( _state === STATE.ZOOM && !_this.noZoom ) {
            
            _zoomStart.copy( getMouseOnScreen( event.pageX, event.pageY ) );
            _zoomEnd.copy(_zoomStart);
            
        } else if ( _state === STATE.PAN && !_this.noPan ) {
            
            _panStart.copy( getMouseOnScreen( event.pageX, event.pageY ) );
            _panEnd.copy(_panStart);
            
        }
        
        document.addEventListener( 'mousemove', mousemove, false );
        document.addEventListener( 'mouseup', mouseup, false );
        
        _this.dispatchEvent( startEvent );
        
    }
    
    function mousemove( event ) {
        
        if ( _this.enabled === false ) return;
        
        event.preventDefault();
        event.stopPropagation();
        
        if ( _state === STATE.ROTATE && !_this.noRotate ) {
            
            _movePrev.copy(_moveCurr);
            _moveCurr.copy( getMouseOnCircle( event.pageX, event.pageY ) );
            
        } else if ( _state === STATE.ZOOM && !_this.noZoom ) {
            
            _zoomEnd.copy( getMouseOnScreen( event.pageX, event.pageY ) );
            
        } else if ( _state === STATE.PAN && !_this.noPan ) {
            
            _panEnd.copy( getMouseOnScreen( event.pageX, event.pageY ) );
            
        }
        
    }
    
    function mouseup( event ) {
        
        if ( _this.enabled === false ) return;
        
        event.preventDefault();
        event.stopPropagation();
        
        _state = STATE.NONE;
        
        document.removeEventListener( 'mousemove', mousemove );
        document.removeEventListener( 'mouseup', mouseup );
        _this.dispatchEvent( endEvent );
        
    }
    
    function mousewheel( event ) {
        if(!_this.noZoom) {
        
        if ( _this.enabled === false ) return;
        
        event.preventDefault();
        event.stopPropagation();
        
        var delta = 0;
        
        if ( event.wheelDelta ) { // WebKit / Opera / Explorer 9
            
            delta = event.wheelDelta / 40;
            
        } else if ( event.detail ) { // Firefox
            
            delta = - event.detail / 3;
            
        }
        
        _zoomStart.y += delta * 0.01;
        _this.dispatchEvent( startEvent );
        _this.dispatchEvent( endEvent );
        }
        
    }
    
    function touchstart( event ) {
        
        if ( _this.enabled === false ) return;
        
        switch ( event.touches.length ) {
                
            case 1:
                _state = STATE.TOUCH_ROTATE;
                _moveCurr.copy( getMouseOnCircle( event.touches[ 0 ].pageX, event.touches[ 0 ].pageY ) );
                _movePrev.copy(_moveCurr);
                break;
                
            case 2:
                _state = STATE.TOUCH_ZOOM_PAN;
                var dx = event.touches[ 0 ].pageX - event.touches[ 1 ].pageX;
                var dy = event.touches[ 0 ].pageY - event.touches[ 1 ].pageY;
                _touchZoomDistanceEnd = _touchZoomDistanceStart = Math.sqrt( dx * dx + dy * dy );
                
                var x = ( event.touches[ 0 ].pageX + event.touches[ 1 ].pageX ) / 2;
                var y = ( event.touches[ 0 ].pageY + event.touches[ 1 ].pageY ) / 2;
                _panStart.copy( getMouseOnScreen( x, y ) );
                _panEnd.copy( _panStart );
                break;
                
            default:
                _state = STATE.NONE;
                
        }
        _this.dispatchEvent( startEvent );
        
        
    }
    
    function touchmove( event ) {
        
        if ( _this.enabled === false ) return;
        
        event.preventDefault();
        event.stopPropagation();
        
        switch ( event.touches.length ) {
                
            case 1:
                _movePrev.copy(_moveCurr);
                _moveCurr.copy( getMouseOnCircle(  event.touches[ 0 ].pageX, event.touches[ 0 ].pageY ) );
                break;
                
            case 2:
                var dx = event.touches[ 0 ].pageX - event.touches[ 1 ].pageX;
                var dy = event.touches[ 0 ].pageY - event.touches[ 1 ].pageY;
                _touchZoomDistanceEnd = Math.sqrt( dx * dx + dy * dy );
                
                var x = ( event.touches[ 0 ].pageX + event.touches[ 1 ].pageX ) / 2;
                var y = ( event.touches[ 0 ].pageY + event.touches[ 1 ].pageY ) / 2;
                _panEnd.copy( getMouseOnScreen( x, y ) );
                break;
                
            default:
                _state = STATE.NONE;
                
        }
        
    }
    
    function touchend( event ) {
        
        if ( _this.enabled === false ) return;
        
        switch ( event.touches.length ) {
                
            case 1:
                _movePrev.copy(_moveCurr);
                _moveCurr.copy( getMouseOnCircle(  event.touches[ 0 ].pageX, event.touches[ 0 ].pageY ) );
                break;
                
            case 2:
                _touchZoomDistanceStart = _touchZoomDistanceEnd = 0;
                
                var x = ( event.touches[ 0 ].pageX + event.touches[ 1 ].pageX ) / 2;
                var y = ( event.touches[ 0 ].pageY + event.touches[ 1 ].pageY ) / 2;
                _panEnd.copy( getMouseOnScreen( x, y ) );
                _panStart.copy( _panEnd );
                break;
                
        }
        
        _state = STATE.NONE;
        _this.dispatchEvent( endEvent );
        
    }
    
    this.domElement.addEventListener( 'contextmenu', function ( event ) { event.preventDefault(); }, false );
    
    this.domElement.addEventListener( 'mousedown', mousedown, false );
    
    this.domElement.addEventListener( 'mousewheel', mousewheel, false );
    this.domElement.addEventListener( 'DOMMouseScroll', mousewheel, false ); // firefox
    
    this.domElement.addEventListener( 'touchstart', touchstart, false );
    this.domElement.addEventListener( 'touchend', touchend, false );
    this.domElement.addEventListener( 'touchmove', touchmove, false );
    
    window.addEventListener( 'keydown', keydown, false );
    window.addEventListener( 'keyup', keyup, false );
    
    this.handleResize();
    
    // force an update at start
    this.update();
    
};

THREE.TrackballControls.prototype = Object.create( THREE.EventDispatcher.prototype );
THREE.TrackballControls.prototype.constructor = THREE.TrackballControls;