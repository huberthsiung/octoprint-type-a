var scene = null;
var gcode_object = null;
var gcode_layer = null;
var bg = null;
var curr_model = null;
var render_progresses = "";
var model_count = 0;
var curr_model_name = '';
var layer_counter = 0;
var layer_counter3d = 0;
var snap_gap = 800;
var done_render = false;
var stop_rendering = false;

function processSlider() {
    if(view_mode_3d) {
        for(var lid in gcode_layers) {
            var layer = gcode_layers[lid];
            if(lid < layer_counter3d) {
                for(var tid in layer.type) {
                    var type = layer.type[tid];
                    if(layer_counter3d == gcode_layers.length)
                        type.material.opacity = type.extruding ? 0.7 : 0.4;
                    else
                        type.material.opacity = type.extruding ? 0.2 : 0.1;
                    type.material.transparent = true;
                    type.material.linewidth = 1;
                    if(!curr_model.extrude && (type.material.opacity == 0.1 || type.material.opacity == 0.4))
                        type.material.visible = false;
                    else
                        type.material.visible = true;
                }
            } else if(lid > layer_counter3d) {
                for(var tid in layer.type) {
                    var type = layer.type[tid];
                    type.material.visible = false;
                }
            } else if(lid == layer_counter3d) {
                for(var tid in layer.type) {
                    var type = layer.type[tid];
                    type.material.opacity = type.extruding ? 1.0 : 0.9;
                    type.material.transparent = false;
                    type.material.linewidth = 5;
                    if(!curr_model.extrude && type.material.opacity == 0.9)
                        type.material.visible = false;
                    else
                        type.material.visible = true;
                }
            }
        }
    } else {
        if(gcode_object) {
            var selectedObject = scene.getObjectByName(gcode_object.name);
            scene.remove( selectedObject );
        }
        openGCodeLayer(layer_counter, curr_model_name);
    }
}

function openGCodeFromText(name) {
    if(!curr_model) {
        console.log("no model to open");
        return false;
    }
    if(gcode_layer)
        remove_layer();
    
    processSlider();
    
    if(!gcode_object) {
        gcode_object = curr_model.createObjectFromGCode();
        gcode_object.name = name;
    }
    scene.add(gcode_object);
}
function openGCodeLayer(layer_num, name) {
    if(!curr_model) {
        console.log("can't open layer; model doesn't exist");
        return false;
    }
    
    if(gcode_layer) {
        if(gcode_layer.name == (name + layer_num)) {
            return;
        } else
            remove_layer();
    }
    
    if(layer_storage[layer_num]) {
        gcode_layer = layer_storage[layer_num];
    } else {
        gcode_layer = curr_model.createObjectFromGCodeLayer(layer_num);
        gcode_layer.name = name + layer_num;
        layer_storage[layer_num] = gcode_layer.clone();
    }
    scene.add(gcode_layer);
}
function remove_object(){
    var selectedObject = scene.getObjectByName(gcode_object.name);
    scene.remove( selectedObject );
    gcode_object = null;
}
function remove_layer(){
    var selectedLayer = scene.getObjectByName(gcode_layer.name);
    scene.remove( selectedLayer );
    gcode_layer = null;
}
function remove_bg(){
    var selectedBg = scene.getObjectByName(bg.name);
    scene.remove( selectedBg );
    bg = null;
}
function render_3dpreview(file_loaded) {
    model_count++;
    if(model_count > 1) {
        console.log("Error: already rendering something else!!!");
        model_count--;
        return false;
    }
    
    render_progresses = "";
    layer_storage = [];
    view_mode_3d = true;
    snap_gap = 800;
    
    var transform_styles = ['-webkit-transform',
                            '-moz-transform',
                            '-ms-transform',
                            'transform'];
    for(i in transform_styles) {
        $('.ppc-progress-fill').css(transform_styles[i],'rotate(0deg)');
    }
    $('.progress-pie-chart').removeClass('gt-50');
    $('.ppc-percents span').html(0+'%');
    
    if(typeof file_loaded.name == 'undefined')
        file_name = $("#file_loaded").html();
    else
        file_name = file_loaded.name;
    
    if(!file_name)
        return false;
    render_progresses = file_name+"<br>Rendering...";
    
    if(gcode_object !== null) {
        remove_object();
        bg.material.visible = false;
        document.getElementById("toggleTravelBtn").style.display = "none";
        document.getElementById("resetPosBtn").style.display = "none";
        document.getElementById("toggleSnapBtn").style.display = "none";
        document.getElementById("UIBar").style.display = "none";
        document.getElementById("DimensionView").style.display = "none";
        document.getElementById("toggle23DBtn").style.display = "none";
        document.getElementById("incLayerBtn").style.display = "none";
        document.getElementById("decLayerBtn").style.display = "none";
        $('#layer_slider').hide()
        
        //images defaults
        document.getElementById("snapImg").src = "/static/img/object_unlock.svg";
        document.getElementById("travelImg").src = "/static/img/travel_hide.svg";
        document.getElementById("23DImg").src = "/static/img/layer_all.svg";
    }
    
    if(gcode_layer !== null) {
        remove_layer();
    }
    
    if(!scene) {
        scene = createScene($('#renderArea'));
    }

    if(stop_rendering) {
        setTimeout(function(){
                   document.getElementById("modelNotRendered").style.display = "none";
                   document.getElementById("LoadingImage").style.display = "block";
                   stop_rendering = false;
                   xhr_stuff(file_name);
                   },1151);
    } else {
        document.getElementById("modelNotRendered").style.display = "none";
        document.getElementById("LoadingImage").style.display = "block";
        xhr_stuff(file_name);
    }
}
function xhr_stuff(name) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', '' + BASEURL + 'downloads/files/local/' + name, true);
    xhr.responseType = 'blob';
    
    xhr.onload = function(e) {
        curr_model = new Model();
        var currFile = xhr.response;
        currFile.lastModifiedDate = new Date();
        currFile.name = name;
        
        var ind = 0;
        var nav = new FileNavigator(currFile);
        
        $(document).ready(function(){
                          $('#resetPosBtn').tooltip({delay: {show: 500, hide: 100}});
                          $('#toggleSnapBtn').tooltip({delay: {show: 500, hide: 100}});
                          $('#toggleTravelBtn').tooltip({delay: {show: 500, hide: 100}});
                          $('#toggle23DBtn').tooltip({delay: {show: 500, hide: 100},placement: "right"});
        });

        for(i in transform_styles) {
            $('.ppc-progress-fill').css(transform_styles[i],'');
        }
        
        var xprogress = -1;
        var xdeg = -1;
        done_render = false;
        var w8time = 0;
        var transform_styles = ['-webkit-transform',
                                '-moz-transform',
                                '-ms-transform',
                                'transform'];
        
        nav.readSomeLines(ind, function linesReadHandler(err, index, lines, eof, progress) {
            if (err) {
                console.log("error reading lines of file");
                return;
            }
            if(stop_rendering) {
                stop_rendering = false;
                return;
            }

            curr_model.parse_code(lines);
            
            w8time = 0;
            
            var stall = false;
            
            var $ppc = $('.progress-pie-chart');
            if($ppc.hasClass('gt-50'))
                $ppc.removeClass('gt-50');
            var deg = Math.floor(360*progress/100);
            stall = deg==xdeg?true:false;
            
            if (progress > 50) {
                if(xprogress >= 50)
                    $ppc.addClass('gt-50');
                if(xprogress < 50) {
                    w8time = 550;
                }
            }
            
            if(!stall) {
                if(progress > 50 && xprogress < 50) {
                    for(i in transform_styles)
                        $('.ppc-progress-fill').css(transform_styles[i],'rotate(180deg)');
                    setTimeout(function(){
                        if($ppc.hasClass('gt-50'))
                            $ppc.removeClass('gt-50');
                        $ppc.addClass('gt-50');
                        for(i in transform_styles)
                            $('.ppc-progress-fill').css(transform_styles[i],'rotate('+ deg +'deg)');
                        done_render = true;
                        $('.ppc-percents span').html(progress+'%');
                    }, 550);
                } else {
                    if(xprogress == 50)
                        done_render = true;
                    for(i in transform_styles)
                        $('.ppc-progress-fill').css(transform_styles[i],'rotate('+ deg +'deg)');
                }
            
                if(progress > 50 && xprogress < 50)
                    $('.ppc-percents span').html('50%');
                else
                    $('.ppc-percents span').html(progress+'%');
            
            xprogress = progress;
            xdeg = deg;
            }
            
            if (eof) {
                setTimeout(function(){
                    if(stop_rendering) {
                        stop_rendering = false;
                        return;
                    }
                
                    curr_model_name = name;
                
                    layer_counter = gcode_layers.length - 1;
                    layer_counter3d = gcode_layers.length;
                
                    top_controls.noZoom = true;
                    top_controls.noPan = true;
                    controls.noRotate = false;
                    controls.noZoom = false;
                    controls.noPan = false;
                    openGCodeFromText( name );
                    controls.reset();
                    top_controls.reset();
                                             
                
                    // Create Background
                    if(bg)
                        remove_bg();
                    var bggeo = new THREE.PlaneGeometry(305 * curr_model.scale, 305 * curr_model.scale, 0);
                    var bgmat = new THREE.MeshBasicMaterial({map: bgTex, side: THREE.DoubleSide});
                    bg = new THREE.Mesh(bggeo,bgmat);
                    bg.material.transparent = true;
                    bg.material.opacity = 1.0;
                    bg.material.color.setHex( 0xffffff );
                    bg.name = "backgroundplatescalable";
                    bg.position.z = curr_model.object.position.z-1;
                    scene.add(bg);
                
                    $('#layerControl').slider({
                                formatter: function(value) {
                                    if(value < gcode_layers.length)
                                        return 'Layer Number: ' + value;
                                    else
                                        return 'Showing Full Model';
                                    },
                                min: 0,
                                max: gcode_layers.length,
                                value: gcode_layers.length,
                                orientation: 'vertical',
                                reversed: true
                            });
                          
                    $('#layerControl').data('slider').max = gcode_layers.length;
                    $('#layerControl').data('slider').value = gcode_layers.length;
                    $('#layerControl').data('slider').setValue(gcode_layers.length,true);
                          
                    $('#layerControl').on("slide", function(slideEvt) {
                                if(slideEvt.value < gcode_layers.length) {
                                    layer_counter = slideEvt.value;
                                    if(!view_mode_3d) {
                                        openGCodeLayer(layer_counter, curr_model_name);
                                    }
                                } else {
                                    layer_counter = slideEvt.value-1;
                                    if(!view_mode_3d) {
                                        openGCodeLayer(layer_counter, curr_model_name);
                                    }
                                }
                                layer_counter3d = slideEvt.value;
                                processSlider();
                                if(curr_model.snap && view_mode_3d) {
                                    updateSnapCamera();
                                }
                            });
                
                    $('#settings-modelColor1').on("select", function() {
                                for(var lid in gcode_layers) {
                                    var layer = gcode_layers[lid];
                                    for(var tid in layer.type) {
                                        var type = layer.type[tid];
                                        if(type.extruding) {
                                            type.color = new THREE.Color($(this).val());
                                            for(var i = 0; i < type.geometry.colors.length; i++)
                                                type.geometry.colors[i].setHex(type.color.getHex());
                                            type.geometry.colorsNeedUpdate = true;
                                        }
                                    }
                                }
                            });
                    $('#settings-modelColor2').on("select", function() {
                                for(var lid in gcode_layers) {
                                    var layer = gcode_layers[lid];
                                    for(var tid in layer.type) {
                                        var type = layer.type[tid];
                                        if(!type.extruding) {
                                            type.color = new THREE.Color($(this).val());
                                            for(var i = 0; i < type.geometry.colors.length; i++)
                                                type.geometry.colors[i].setHex(type.color.getHex());
                                            type.geometry.colorsNeedUpdate = true;
                                        }
                                    }
                                }
                            });
                          
                    model_count--;
                
                    render_progresses = "";
                    document.getElementById("LoadingImage").style.display = "none";

                    document.getElementById("toggleTravelBtn").style.display = "block";
                    document.getElementById("toggleSnapBtn").style.display = "block";
                    document.getElementById("toggle23DBtn").style.display = "block";
                    bg.material.visible = true;
                    $('#layer_slider').show();
                    document.getElementById("UIBar").style.display = "block";
                    document.getElementById("DimensionView").style.display = "block";
                    document.getElementById("resetPosBtn").style.display = "block";
                    document.getElementById("incLayerBtn").style.display = "block";
                    document.getElementById("decLayerBtn").style.display = "block";
                }, 520+w8time);
                
                return;
            }
            if(stall) {
                if(progress < 100 && (progress <= 50 || done_render)) {
                    nav.readSomeLines(index + lines.length, linesReadHandler);
                }
            } else {
                setTimeout(function(){
                    if(progress < 100 && (progress <= 50 || done_render)) {
                        nav.readSomeLines(index + lines.length, linesReadHandler);
                    }
                }, 520+w8time);
            }
        });
        
    }

    xhr.send();
}

function stop_render() {
    stop_rendering = true;
    
    model_count = 0;
    layer_storage = [];
    view_mode_3d = true;
    snap_gap = 800;

    clean_model();
    
    var transform_styles = ['-moz-transform',
                            '-ms-transform',
                            'transform',
                            '-webkit-transform'];
    
    var $ppc = $('.progress-pie-chart');
    for(i in transform_styles) {
        $('.ppc-progress-fill').css(transform_styles[i],'rotate(0deg)');
    }
    if($ppc.hasClass('gt-50'))
        $('.progress-pie-chart').removeClass('gt-50');
    $('.ppc-percents span').html(0+'%');
}

function clean_model() {
    if(gcode_object !== null) {
        remove_object();
        bg.material.visible = false;
        document.getElementById("toggleTravelBtn").style.display = "none";
        document.getElementById("resetPosBtn").style.display = "none";
        document.getElementById("toggleSnapBtn").style.display = "none";
        document.getElementById("UIBar").style.display = "none";
        document.getElementById("DimensionView").style.display = "none";
        document.getElementById("toggle23DBtn").style.display = "none";
        document.getElementById("incLayerBtn").style.display = "none";
        document.getElementById("decLayerBtn").style.display = "none";
        $('#layer_slider').hide()
        
        //images defaults
        document.getElementById("snapImg").src = "/static/img/object_unlock.svg";
        document.getElementById("travelImg").src = "/static/img/travel_hide.svg";
        document.getElementById("23DImg").src = "/static/img/layer_all.svg";
    }
    
    if(gcode_layer !== null) {
        remove_layer();
    }
    
    document.getElementById("modelNotRendered").style.display = "block";
    document.getElementById("LoadingImage").style.display = "none";
    render_progresses = "";
}

function increase_layer() {
    if(layer_counter3d < gcode_layers.length) {
        if(layer_counter < gcode_layers.length-1)
            layer_counter++;
        layer_counter3d++;
        $('#layerControl').data('slider').value = layer_counter3d;
        $('#layerControl').data('slider').setValue(layer_counter3d,true);
        if(view_mode_3d) {
            processSlider();
            if(curr_model.snap)
                updateSnapCamera();
        } else
            openGCodeLayer(layer_counter, curr_model_name);
    }
}

function decrease_layer() {
    if(layer_counter3d > 0) {
        if(layer_counter3d == layer_counter)
            layer_counter--;
        layer_counter3d--;
        $('#layerControl').data('slider').value = layer_counter3d;
        $('#layerControl').data('slider').setValue(layer_counter3d,true);
        if(view_mode_3d) {
            processSlider();
            if(curr_model.snap)
                updateSnapCamera();
        } else
            openGCodeLayer(layer_counter, curr_model_name);
    }
}

function updateSnapCamera() {
    if(!view_mode_3d) {
        console.log("Error switching camera snapping in 2d mode");
        return false;
    }

    if(curr_model.snap) {
        if(layer_counter3d < gcode_layers.length) {
            var world_z = gcode_layers[layer_counter3d].z * curr_model.scale + gcode_object.position.z + curr_model.world_topz / 2 + snap_gap;
            controls.object.position.z = world_z;
        } else {
            var world_z = gcode_layers[gcode_layers.length-1].z * curr_model.scale + gcode_object.position.z + curr_model.world_topz / 2 + snap_gap;
            controls.object.position.z = world_z;
        }
    }
}

function toggleSnap() {
    curr_model.snap = !curr_model.snap;
    
    controls.reset();
    if(curr_model.snap) {
        controls.noRotate = true;
        snap_gap = 800;
        controls.object.position.x = 0;
        controls.object.position.y = 0;
        document.getElementById("snapImg").src = "/static/img/object_locked.svg";
        $('#toggleSnapBtn').attr('title', 'Unsnap Camera').tooltip('fixTitle').tooltip('show');
    } else {
        controls.noRotate = false;
        document.getElementById("snapImg").src = "/static/img/object_unlock.svg";
        $('#toggleSnapBtn').attr('title', 'Snap Camera').tooltip('fixTitle').tooltip('show');
    }
    
    var line_array = gcode_object.getDescendants();
    for(var cid in  line_array) {
        var child = line_array[cid];
        var child_z = (child.geometry.vertices[0].z * curr_model.scale) + gcode_object.position.z;
        var z_dif = controls.object.position.z - child_z;
        if(z_dif < 0) {
            child.renderDepth = (-z_dif);
        } else if(z_dif > 0) {
            child.renderDepth = z_dif;
        } else {
            child.renderDepth = 0;
        }
    }
    
    updateSnapCamera();
}

function toggle23D() {
    if(view_mode_3d) {
        
        controls.noRotate = true;
        controls.noZoom = true;
        controls.noPan = true;
        top_controls.reset();
        if(curr_model.snap) {
            top_controls.object.position.copy(controls.object.position);
            top_controls.object.position.z = curr_model.world_topz / 2 + snap_gap;
            top_controls.target.copy(controls.target);
        }
        bg.material.visible = false;
        $('#toggle23DBtn').attr('title', 'Switch to 3D View').tooltip('fixTitle').tooltip('show');
        document.getElementById("23DImg").src = "/static/img/layer_single.svg";
        document.getElementById("toggleSnapBtn").style.display = "none";
        document.getElementById("toggleTravelBtn").style.display = "none";
        
        if(gcode_layers.length > 0) {
            for(var lid in gcode_layers) {
                var layer = gcode_layers[lid];
                for(var tid in layer.type) {
                    var type = layer.type[tid];
                    type.material.transparent = true;
                    type.material.linewidth = 5;
                    type.material.opacity = 1;
                    type.material.visible = true;
                }
            }
        }
    } else {
        top_controls.noZoom = true;
        top_controls.noPan = true;
        if(curr_model.snap) {
            controls.object.position.x = top_controls.object.position.x;
            controls.object.position.y = top_controls.object.position.y;

            snap_gap = top_controls.object.position.z - curr_model.world_topz / 2;
            controls.target.copy(top_controls.target);
        }
        
        bg.material.visible = true;
        $('#toggle23DBtn').attr('title', 'Switch to 2D View').tooltip('fixTitle').tooltip('show');
        document.getElementById("23DImg").src = "/static/img/layer_all.svg";
        document.getElementById("toggleSnapBtn").style.display = "block";
        document.getElementById("toggleTravelBtn").style.display = "block";
        
        $('#layerControl').data('slider').value = layer_counter3d;
        $('#layerControl').data('slider').setValue(layer_counter3d,true);
    }
    
    view_mode_3d = !view_mode_3d;
    
    if(view_mode_3d) {
        openGCodeFromText(curr_model_name);
        if(!curr_model.snap) {
            controls.noRotate = false;
        }
        controls.noZoom = false;
        controls.noPan = false;
        processSlider();
        updateSnapCamera();
        if(curr_model.extrude)
            curr_model.showEverything();
        else
            curr_model.showOnlyExtrusion();
    } else {
        if(gcode_object) {
            var selectedObject = scene.getObjectByName(gcode_object.name);
            scene.remove( selectedObject );
        }
        openGCodeLayer(layer_counter, curr_model_name);
        top_controls.noZoom = false;
        top_controls.noPan = false;
    }
}

function toggle_extrude() {
    if(!curr_model) {
        console.log("error toggle extrusion; model is not loaded!");
        return false;
    }
    curr_model.extrude = !curr_model.extrude;
    
    if(curr_model.extrude) {
        curr_model.showEverything();
        document.getElementById("travelImg").src = "/static/img/travel_hide.svg" ;
        $('#toggleTravelBtn').attr('title', 'Hide Travel').tooltip('fixTitle').tooltip('show');
    } else {
        curr_model.showOnlyExtrusion();
        document.getElementById("travelImg").src = "/static/img/travel_show.svg" ;
        $('#toggleTravelBtn').attr('title', 'Show Travel').tooltip('fixTitle').tooltip('show');
    }
    
    processSlider();
}

function reset_model_position() {
    if(!curr_model) {
        console.log("error resetting model position; model is not loaded!");
        return false;
    }
    if(!scene) {
        console.log("error resetting model position; scene has not been created!");
        return false;
    }
    
    if(view_mode_3d) {
        controls.reset();
        if(curr_model.snap) {
            controls.noRotate = true;
            snap_gap = 800;
            controls.object.position.x = 0;
            controls.object.position.y = 0;
            updateSnapCamera();
        }
    } else {
        snap_gap = 800;
        top_controls.reset();
        top_controls.object.position.z = snap_gap + curr_model.world_topz / 2;
    }
    
}

$(function() {


  if (!Modernizr.webgl) {
    alert('Sorry, you need a WebGL capable browser to use this.\n\nGet the latest Chrome or FireFox.');
    return;
  }

  if (!Modernizr.localstorage) {
    alert("Man, your browser is ancient. I can't work with this. Please upgrade.");
    return;
  }

});

