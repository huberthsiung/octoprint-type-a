/**
 * Parses a string of gcode instructions, and invokes handlers for
 * each type of command.
 *
 * Special handler:
 *   'default': Called if no other handler matches.
 */
function GCodeParser(handlers) {
  this.handlers = handlers || {};
}

function validateGcode(text, arr) {
  var isThere = ( text.indexOf(arr[0]) > -1 )
   if( arr.length > 0 && !isThere ){
      arr.shift();
      return validateGcode(text,arr);
    }else{
      return isThere;
    }
};

GCodeParser.prototype.parseLine = function(text, info) {
  var valid_gcodes =  ["G1","G0","G21","G90","G91","G92","M82","M84"];
  
  text = text.replace(/;.*$/, '').trim(); // Remove comments
  text = text.replace(/\s\s+/g, ' ').trim(); // Remove double spaces

  in_list = validateGcode(text,valid_gcodes);
  
    if (!in_list) {
        return;
    //text = text.trim();
    }
  
  if (text && in_list) {
    var tokens = text.split(' ');
    if (tokens) {
      var cmd = tokens[0];
      var args = {
        'cmd': cmd
      };
      tokens.splice(1).forEach(function(token) {
        var key = token[0].toLowerCase();
        var value = parseFloat(token.substring(1));
        args[key] = value;
      });
      var handler = this.handlers[tokens[0]] || this.handlers['default'];
      if (handler) {
        return handler(args, info);
      }
    }
  }
};

GCodeParser.prototype.parse = function(gcode) {
  for (var i = 0; i < gcode.length; i++) {
    if (stop_rendering) {
        console.log("breaking parse");
        break;
    }
      
    if (this.parseLine(gcode[i], i) === false) {
      break;
    }
  }

};
