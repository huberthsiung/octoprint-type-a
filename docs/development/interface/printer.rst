.. _sec-development-interface-plugin:

``octoprint.printer``
---------------------

.. automodule:: octoprint.printer
   :members: Printer
   :undoc-members:
